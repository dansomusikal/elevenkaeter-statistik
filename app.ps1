function Install-Python () {
   wget https://www.python.org/ftp/python/3.11.6/python-3.11.6-amd64.exe -outfile pyinstaller.exe
   ./pyinstaller.exe /passive PrependPath=1 Include_test=0
   Remove-Item pyinstaller.exe
}
function Check-Command($cmdname)
{
   return [bool](Get-Command -Name $cmdname -ErrorAction SilentlyContinue)
}

if (Check-Command -cmdname py) { } else { Install-Python }

$py = py --list
$outcome = 0
for($i=0;$i -lt $py.Length; $i++) { if($py[$i].Contains("3.11")) { Write-Host "Success";$outcome=1;break } }
if ($outcome -eq 0) { Install-Python }
py -3.11 -m pip install -r requirements.txt
py -3.11 app.py
