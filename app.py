import pandas
import matplotlib
from matplotlib import pyplot as plt
import os
from pathlib import Path
import datetime
current_year = datetime.date.today().year

cwd = os.getcwd()
os.mkdir("VT"+str(current_year)[-2:])
os.chdir(Path(cwd+"/VT"+str(current_year)[-2:]))

d = pandas.read_excel("D:\Dans o Musikal i Lund AB\Kansli - Dokument\Kvalitetsarbete\Sammanställning_Elevenkäter\Översikt_elevenkäter.xlsx")

d = d.sort_values(by=['Unnamed: 0','Unnamed: 1'])

#colors = {'D14':'b','D15':'g','D16':'r','D17':'c','D18':'m','D19':'y','D20':'springgreen','D21':'sienna','D22':'orange','D23':'cornflowerblue','D24':'blueviolet','D25':'lightcoral',
#          'M14':'b','M15':'g','M16':'r','M17':'c','M18':'m','M19':'y','M20':'springgreen','M21':'sienna','M22':'orange','M23':'cornflowerblue','M24':'blueviolet','M25':'lightcoral',
#          'alla klasser':'dimgrey'}
#

colors = {}

available = ['b','g','r','c','m','y','springgreen','sienna','orange','cornflowerblue','blueviolet','lightcoral']

available = available[:int((len(sorted(set(list(d['Unnamed: 1']))))-1)/2)]

print(available)

available.extend(available)

available.append('dimgrey')

for c in sorted(set(list(d['Unnamed: 1']))):
    colors[c] = available[0]
    available.remove(available[0])

print(colors)

for c in d.columns[2:]:
    d.plot(x= 'Unnamed: 0', y=c,kind='bar', color=[colors[i] for i in d['Unnamed: 1']])
    plt.legend(bbox_to_anchor=(1,0.5), loc="center left", handles=[matplotlib.patches.Patch(color=colors[i], label=i) for i in list(set(d['Unnamed: 1']))])
    plt.title(c)
    plt.savefig((c+" per klass"+".png").replace("?","").replace("/"," "),bbox_inches='tight')
    plt.close()

    d[d['Unnamed: 1'] == "alla klasser"].plot(x= 'Unnamed: 0', y=c, color=[colors[i] for i in d['Unnamed: 1']], legend=False)
    plt.title(c)
    plt.savefig((c+".png").replace("?","").replace("/"," "),bbox_inches='tight')
    plt.close()

# Nya svarsalternativ skolhälsovård
results = {}
for topic in ["283 Hur upplever du din tillgång till skolsköterskan?",
              "284 Hur upplever du din tillgång till sjukgymnasten?",
              "285 Hur upplever du din tillgång till skolans kurator?"]:
    results[topic] = {}
    for year in range(2024,current_year + 1):
        df = pandas.read_excel("D:\Dans o Musikal i Lund AB\Kansli - Dokument\Kvalitetsarbete\Sammanställning_Elevenkäter\Översikt_elevenkäter.xlsx", sheet_name=f"{year} - data")
        q = df[topic].tolist()
        results[topic][year] = {k: q.count(k) for k in set(q)}

    table = {str(year):[results[topic][year][k] for k in results[topic][year]] for year in results[topic]}
    headers = [k for k in results[topic][2024]]

    output = pandas.DataFrame.from_dict(table, orient='index', columns=headers)

    totals = output.sum(axis=1)
    percent = output.div(totals, axis=0).mul(100).round(2)
    ax = percent.plot(kind='barh', stacked=True, figsize=(9, 5), xticks=[])
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), ncol=2, frameon=False)
    ax.tick_params(left=False, bottom=False)
    ax.spines[['top', 'bottom', 'left', 'right']].set_visible(False)
    for c in ax.containers:
        # custom label calculates percent and add an empty string so 0 value bars don't have a number
        labels = [f'{w:0.2f}%' if (w := v.get_width()) > 0 else '' for v in c]
        # add annotations
        ax.bar_label(c, labels=labels, label_type='center', padding=0.3, color='w')

    plt.title(topic)
    plt.savefig((topic+".png").replace("?","").replace("/"," "), bbox_inches='tight')
    plt.close()